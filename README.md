# Demonstrate the setup of Julia on Gitlab for CI

[![pipeline status](https://gitlab.com/tkpapp/GitlabJuliaDemo.jl/badges/master/pipeline.svg)](https://gitlab.com/tkpapp/GitlabJuliaDemo.jl/commits/master)
[![coverage report](https://gitlab.com/tkpapp/GitlabJuliaDemo.jl/badges/master/coverage.svg)](https://gitlab.com/tkpapp/GitlabJuliaDemo.jl/commits/master)
[![codecov](https://codecov.io/gl/tkpapp/GitlabJuliaDemo.jl/branch/master/graph/badge.svg)](https://codecov.io/gl/tkpapp/GitlabJuliaDemo.jl)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://tkpapp.gitlab.io/GitlabJuliaDemo.jl/)

A small change to see if this works.

This is a minimal setup for a Julia package in Gitlab that has continuous integration and coverage summary set up.

[.gitlab-ci.yml](.gitlab-ci.yml) uses a Julia docker image to

1. build the package,
2. run tests and process coverage, emitting a coverage percentage as a summary,
3. generate the documentation using [Documenter.jl](https://github.com/JuliaDocs/Documenter.jl), which is deployed at

    <https://tkpapp.gitlab.io/GitlabJuliaDemo.jl/>

## Coverage percentage

Use the regular expression `\(\d+.\d+\%\) covered` in *Settings 🢥 CI/CD 🢥 General pipelines settings 🢥 Test coverage parsing* to parse coverage percentage.

## Codecov integration

1. Log in to <https://codecov.io>, *authenticating with Gitlab*.

2. Add your repository. You will see your *codecov token*.

3. On Gitlab, under *Settings 🢥 CI/CD 🢥 Variables*, add a `CODECOV_TOKEN` variable with the given value. Make it *masked*.
