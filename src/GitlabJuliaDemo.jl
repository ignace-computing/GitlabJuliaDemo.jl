module GitlabJuliaDemo

export getA

"""
    getA()

A function that returns `:A`.
"""
getA() = :A

end
